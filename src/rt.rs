#![allow(unused_variables)]

#[derive(Default)]
pub struct Am;

impl Am {
  pub fn abs(&self, _: u16) -> Address {
    Address(0)
  }

  pub fn abx(&self, _: u16) -> Address {
    Address(0)
  }

  pub fn aby(&self, _: u16) -> Address {
    Address(0)
  }

  pub fn inx(&self, _: u16) -> Address {
    Address(0)
  }

  pub fn iny(&self, _: u16) -> Address {
    Address(0)
  }

  pub fn zpg(&self, _: u16) -> Address {
    Address(0)
  }

  pub fn zpx(&self, _: u16) -> Address {
    Address(0)
  }

  pub fn zpy(&self, _: u16) -> Address {
    Address(0)
  }
}

#[derive(Default)]
pub struct Op;

impl Op {
  pub fn adc(&mut self, m: impl Into<u8>) {}

  pub fn and(&mut self, m: impl Into<u8>) {}

  pub fn asl(&mut self, address: Address) {}

  pub fn asl_a(&mut self) {}

  pub fn bit(&mut self, m: impl Into<u8>) {}

  pub fn brk(&mut self, m: impl Into<u8>) {}

  pub fn bcc(&mut self, _: u16) {}

  pub fn bcs(&mut self, _: u16) {}

  pub fn beq(&mut self, _: u16) {}

  pub fn bne(&mut self, _: u16) {}

  pub fn bmi(&mut self, _: u16) {}

  pub fn bpl(&mut self, _: u16) {}

  pub fn bvc(&mut self, _: u16) {}

  pub fn bvs(&mut self, _: u16) {}

  pub fn clc(&mut self) {}

  pub fn cld(&mut self) {}

  pub fn cli(&mut self) {}

  pub fn clv(&mut self) {}

  pub fn cmp(&mut self, m: impl Into<u8>) {}

  pub fn cpx(&mut self, m: impl Into<u8>) {}

  pub fn cpy(&mut self, m: impl Into<u8>) {}

  pub fn dec(&mut self, address: Address) {}

  pub fn dex(&mut self) {}

  pub fn dey(&mut self) {}

  pub fn eor(&mut self, m: impl Into<u8>) {}

  pub fn inc(&mut self, address: Address) {}

  pub fn inx(&mut self) {}

  pub fn iny(&mut self) {}

  pub fn jmp(&mut self, _: u16) -> ! {
    loop {}
  }

  pub fn jsr(&mut self, _: u16) {}

  pub fn lda(&mut self, m: impl Into<u8>) {}

  pub fn ldx(&mut self, m: impl Into<u8>) {}

  pub fn ldy(&mut self, m: impl Into<u8>) {}

  pub fn lsr(&mut self, address: Address) {}

  pub fn lsr_a(&mut self) {}

  pub fn nop(&mut self) {}

  pub fn ora(&mut self, m: impl Into<u8>) {}

  pub fn pha(&mut self) {}

  pub fn php(&mut self) {}

  pub fn pla(&mut self) {}

  pub fn plp(&mut self) {}

  pub fn rol(&mut self, address: Address) {}

  pub fn rol_a(&mut self) {}

  pub fn ror(&mut self, address: Address) {}

  pub fn ror_a(&mut self) {}

  pub fn rti(&mut self) -> ! {
    loop {}
  }

  pub fn rts(&mut self) -> ! {
    loop {}
  }

  pub fn sbc(&mut self, m: impl Into<u8>) {}

  pub fn sec(&mut self) {}

  pub fn sed(&mut self) {}

  pub fn sei(&mut self) {}

  pub fn sta(&mut self, address: Address) {}

  pub fn stx(&mut self, address: Address) {}

  pub fn sty(&mut self, address: Address) {}

  pub fn tax(&mut self) {}

  pub fn tay(&mut self) {}

  pub fn txa(&mut self) {}

  pub fn tya(&mut self) {}

  pub fn tsx(&mut self) {}

  pub fn txs(&mut self) {}
}

pub struct Address(u16);

impl Into<u8> for Address {
  fn into(self) -> u8 {
    todo!()
  }
}
