pub mod game;
pub mod rt;

pub fn main() {
  let mut game = game::Game::default();
  let mut op = rt::Op::default();
  let mut am = rt::Am::default();
  game.res(&mut op, &mut am);
}
